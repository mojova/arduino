## Web Server Stub code

Sample web server that delivers dummy temperature data in **JSON** format.

### <server>/getTemps

Retrieves the current temperature and humidity data.

Response:

```
{
  "temperature": 25,
  "humidity": 68
}
```

### <server>/getData

Retrieves all data related to the Arduino unit

Response:

```
{
  "temperature": 25,
  "humidity": 68,
  "features": {
    "pihalamppu": {
      "state": "off"
    },
    "otsalamppu": {
      "state": "on"
    }
  }
}
```

### <server>/toggelAction?target=<target>&state=<state>

TODO...