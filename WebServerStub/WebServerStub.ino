#include <SPI.h>
#include <Ethernet.h>

// Defines the debug state so the program will use serial prints for debug messages
// comment out when in production
#define DEBUG debug

// Buffer for reading the Request header to determine following actions
#define STRING_BUFFER_SIZE 128
char buffer[STRING_BUFFER_SIZE];

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,110);

// Initialize the Ethernet server library
// with the IP address and port you want to use 
// (port 80 is default for HTTP):
EthernetServer server(80);

void setup() {

#if defined DEBUG
  Serial.begin(9600);
#endif

  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();

#if defined DEBUG
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
#endif
}


void loop() {

  EthernetClient client = server.available();

  if (client) { 
    handleClientConnection(client);
  }
}


/*************************************************
 * 
 * FUNCTION: handleClientConnection
 * 
 * input client    pointer to incoming client connection to arduino
 *
 * Code inspiration for routing from http://playground.arduino.cc/Code/WebServer
 *
 *************************************************/
void handleClientConnection(EthernetClient client) {

#if defined DEBUG
  Serial.println("new client");
#endif

  // Indexes used for buffering the request line
  int bufindex = 0;
  int loadindex = 0;

  // an http request ends with a blank line
  //boolean currentLineIsBlank = true;

  if (client.connected() && client.available()) {

    char c = client.read();

#if defined DEBUG
    Serial.write(c);
#endif

    buffer[0] = c;
    buffer[1] = c;
    bufindex = 2;

    while (buffer[bufindex-2] != '\r' && buffer[bufindex-1] != '\n') { // read full row and save it in buffer
      c = client.read();
      if (bufindex<STRING_BUFFER_SIZE) {
        buffer[bufindex] = c;
      }
      bufindex++;
    }

#if defined DEBUG
    Serial.write(buffer);
#endif

    String _buffer = String(buffer);

    /*
      Handler for url /getTemps
     
     Example:
     http://192.168.1.110/getTemps
     Response:
     { "temperature": 25, "humidity": 68 }
     
     */
    if (_buffer.indexOf("getTemps") > 0) {
      sendTemperatureResponseJSON(client);
    } 
    /*
      Handler for url /getData
     
     Example:
     http://192.168.1.110/getData
     
     Response is build with all states from the arduino unit.
     
     Response:
     { "temperature": 24, "humidity": 59, "features": { "pihalamppu": { "state": "off" }, "otsalamppu": { "state": "on" }}}
     
     */
    else if (_buffer.indexOf("getData") > 0 ) {
      sendDataResponseJSON(client);
    }
    /*
      Handler for url /toggleAction?target=<target>&state=<state>
     
     Example to turn pihalamppu's state to ON:
     http://192.168.1.110/toggleAction?target=pihalamppu&state=on
     
     Response is build with the changed state of "lamp".
     
     Response:
     TODO...
     
     */
    else if (_buffer.indexOf("toggleAction")) {
      int targetPos = _buffer.indexOf("?target=");
      int statePos =  _buffer.indexOf("&state=");

      String target = _buffer.substring(targetPos + 8, statePos);
      String state = _buffer.substring(statePos + 7, _buffer.indexOf(" HTTP"));

#if defined DEBUG 
      Serial.print(F("Target \""));
      Serial.print(target);
      Serial.print(F("\" ordered to state \""));
      Serial.print(state);
      Serial.println(F("\""));
#endif

    }
    /*
      If nothing else hits we will print a default HTML response
     */
    else {
      client.print(F("HTTP/1.1 200 OK\n"
        "Content-Type: text/html\n"
        "Connection: close\n\n"
        ));
      client.println(F("<!DOCTYPE HTML>"
        "<html>"
        "<p>Temperature is "));
      client.print(random(20, 29));
      client.println(F("</p>"
        "<p>Humidity is "));
      client.print(random(10, 90));
      client.println(F("</p>"
        "</html>"));
    }
  }

  // give the web browser time to receive the data
  delay(1);
  // close the connection:
  client.stop();

#if defined DEBUG
  Serial.println("client disonnected");
#endif
}

void sendTemperatureResponseJSON (EthernetClient client) {
  printJSONHeaders(client);
  client.print('{');
  printTempHumidJSON(client);
  client.print('}');
}

void sendDataResponseJSON (EthernetClient client) {
  printJSONHeaders(client);
  client.print('{');
  printTempHumidJSON(client);
  client.print(',');
  printFreaturesJSON(client);
  client.print('}');
}

void printJSONHeaders (EthernetClient client) {
  client.print(F("HTTP/1.1 200 OK\n"
    "Server: arduino\n"
    "Cache-Control: no-store, no-cache, must-revalidate\n"
    "Pragma: no-cache\n"
    "Connection: close\n"
    "Content-Type: application/json\n\n"));
}

/*
  Print temperature and humidity JSON block
 */
void printTempHumidJSON (EthernetClient client) {
  client.print(F(" \"temperature\":"));
  client.print(random(20, 29));  // DUMMY DATAA
  client.print(',');
  client.print(F(" \"humidity\": "));
  client.print(random(10, 90));  // DUMMY DATAA
}

/*
  Print device's actions and their states JSON block.
 This would include controllable features such as switches and such
 */
void printFreaturesJSON (EthernetClient client) {
  client.print(F("\"features\": {")); // begin features JSON object
  /* BEGIN DUMMY DATA */
  client.print(F("\"pihalamppu\": { \"state\": \"off\" }, "
    "\"otsalamppu\": { \"state\": \"on\" }"
    ));
  /* END DUMMY DATA */
  client.print('}'); // end features JSON object
}

